package com.conseres.ws.soap.endpoints;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.apache.cxf.feature.Features;

/**
 * Esta anotación es la que determina que es un Web Service.
 * @author JMarin
 */
@WebService 
 /** Esta anotación permite usar la clase LoggingFeature para hacer seguimiento 
 * a la ejecución del webservice en tiempo real 
 */
@Features(features="org.apache.cxf.feature.LoggingFeature") 
public class HelloWs {
	
	@WebMethod
	public String hello() {
		return "Hello";
	}
}
